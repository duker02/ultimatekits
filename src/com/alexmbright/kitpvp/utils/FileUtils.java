package com.alexmbright.kitpvp.utils;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import java.io.*;

public class FileUtils {

    /**
     * Copies a directory to a destination
     *
     * @param source      Directory to copy
     * @param destination Destination to copy source to
     * @throws java.io.IOException
     */
    public static void copyDirectory(File source, File destination) throws IOException {
        if (!destination.mkdirs()) {
            throw new IOException("Files could not be created at \"" + destination.getPath() + "\"");
        }

        File[] files = Preconditions.checkNotNull(source, "Source is null").listFiles();
        for (File file : Preconditions.checkNotNull(files, "Files are null")) {
            if (file.isDirectory()) {
                copyDirectory(file, new File(Preconditions.checkNotNull(destination, "Destination is null"), file.getName()));
            } else {
                copyFile(file, new File(Preconditions.checkNotNull(destination, "Destination is null"), file.getName()));
            }
        }
    }

    /**
     * Copy a file using Google guava's copy method
     *
     * @param source      File to copy
     * @param destination Destination to copy source to
     */
    public static void copyFile(File source, File destination) {
        Preconditions.checkNotNull(source, "Source file is null");
        Preconditions.checkNotNull(destination, "Destination is null");

        try {
            Files.copy(source, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Copy an input stream to a file
     *
     * @param inputStream Input stream to copy
     * @param destination Destination file
     */
    public static void copyStreamToFile(InputStream inputStream, File destination) {
        Preconditions.checkNotNull(inputStream, "Input Stream is null");
        Preconditions.checkNotNull(destination, "Destination is null");

        try {
            OutputStream out = new FileOutputStream(destination);
            byte[] buf = new byte[1024];
            int len;

            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            out.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete a file / directory
     *
     * @param file File / directory to delete
     */
    public static void delete(File file) {
        Preconditions.checkNotNull(file, "File is null");

        if (file.isDirectory()) {
            for (File contents : file.listFiles()) {
                contents.delete();
            }
        }
        file.delete();
    }
}
