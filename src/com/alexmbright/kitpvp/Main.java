package com.alexmbright.kitpvp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.alexmbright.kitpvp.listeners.PlayerListener;
import com.alexmbright.kitpvp.utils.ConfigManager;

public class Main extends JavaPlugin {

	public List<Player> usedkit;
	
	public boolean blood;
	public boolean kit_changing;
	public boolean equip_armor;
	
	public String prefix;
	public String no_changing_kits;
	public String no_permission;
	public String nonexistent_kit;
	public String kit_info;
	
	public List<String> grab_layout;
	public List<String> info_layout;
	public String special_tag;
	
	public ConfigManager configManager;
	
	public File signFile;
	public FileConfiguration signs;
	
	public void onDisable() {
		// do nothing
	}

	public void onEnable() {
		configManager = new ConfigManager(this);
		saveDefaultConfig();
		signFile = new File(getDataFolder(), "signs.yml");
		getConfigManager().addFile(signFile);
		try {
			getConfigManager().firstRun();
		} catch (Exception e) {
			e.printStackTrace();
		}
		signs = getConfigManager().getConfigFile("signs.yml");
		getConfigManager().load();
		getConfigManager().save();
		blood = getConfig().getBoolean("blood-on-hit");
		kit_changing = getConfig().getBoolean("allow-kit-changing");
		equip_armor = getConfig().getBoolean("automatically-equip-armor");
		prefix = ChatColor.translateAlternateColorCodes('&', getConfig().getString("messages.prefix"));
		no_changing_kits = ChatColor.translateAlternateColorCodes('&', getConfig().getString("messages.no-changing-kits"));
		no_permission = ChatColor.translateAlternateColorCodes('&', getConfig().getString("messages.no-permission"));
		nonexistent_kit = ChatColor.translateAlternateColorCodes('&', getConfig().getString("messages.non-existent-kit"));
		kit_info = ChatColor.translateAlternateColorCodes('&', getConfig().getString("messages.kit-info"));
		grab_layout = getConfig().getStringList("sign-layout.grab");
		info_layout = getConfig().getStringList("sign-layout.info");
		special_tag = getConfig().getString("sign-layout.special-tag");
		usedkit = new ArrayList<Player>();
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player p = (Player) sender;
		if ((label.equalsIgnoreCase("kit")) || (label.equalsIgnoreCase("k"))) {
			if (args.length == 0) {
				p.sendMessage(prefix + "�bv�b" + this.getDescription().getVersion() + " �7by �b" + this.getDescription().getAuthors().toString().replace("[", "").replace("]", "") + "�7:");
				p.sendMessage("�b/kit [kitname]�8: �7Receive kit.");
				p.sendMessage("�b/kitinfo [kitname]�8: �7See what comes with kit.");
				if (p.isOp()) {
					p.sendMessage("�b/kitsreload�8: �7Reload KitPvP config.");
				}
			} else if (args.length == 1) {
				if (usedkit.contains(p)) {
					p.sendMessage(prefix + no_changing_kits);
				} else if (!usedkit.contains(p)) {
					if (isKit(args[0])) {
							if (getConfig().getBoolean(
									"kits." + args[0] + ".permission")) {
								if ((p.hasPermission(getConfig().getString(
										"kits." + args[0] + ".permission-node")))
										|| (p.isOp())) {
									giveKit(p, args[0]);
								} else {
									p.sendMessage(prefix + no_permission);
								}
							} else {
								giveKit(p, args[0]);
							}
					} else {
						p.sendMessage(prefix + nonexistent_kit);
					}
				}
			}
		} else if ((label.equalsIgnoreCase("kitinfo"))
				|| (label.equalsIgnoreCase("kinfo"))) {
			if (args.length == 0) {
				p.sendMessage(prefix + " �cWrong syntax! Use \"/kitinfo [kitname]\".");
			} else if (args.length == 1) {
				if (isKit(args[0])) {
					sendKitInfo(p, args[0]);
				} else {
					p.sendMessage(prefix + nonexistent_kit);
				}
			}
		} else if ((label.equalsIgnoreCase("kitsreload")) && (p.isOp())
				&& (args.length == 0)) {
			reloadConfig();
			p.sendMessage(prefix + " �aConfig reloaded!");
		}
		return false;
	}
	
	public boolean isKit(String kit) {
		boolean result = false;
		for (String kit1 : getConfig().getConfigurationSection("kits").getKeys(false))
			if (kit1.equalsIgnoreCase(kit))
				result = true;
		return result;
	}
	
	public void giveKit(Player p, String kit) {
		if (getConfig().getStringList("kits." + kit + ".items") != null) {
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			for (String item : getConfig().getStringList("kits." + kit + ".items")) {
				String[] item1 = item.split("x");
				int amount = Integer.parseInt(item1[0].trim());
				if (equip_armor) {
					if (isHelmet(item1[1].trim())) {
						p.getInventory().setHelmet(getItem(amount, item1[1].trim()));
					} else if (isChestplate(item1[1].trim())) {
						p.getInventory().setChestplate(getItem(amount, item1[1].trim()));
					} else if (isLeggings(item1[1].trim())) {
						p.getInventory().setLeggings(getItem(amount, item1[1].trim()));
					} else if (isBoots(item1[1].trim())) {
						p.getInventory().setBoots(getItem(amount, item1[1].trim()));
					} else {
						p.getInventory().addItem(getItem(amount, item1[1].trim()));
					}
				} else {
					p.getInventory().addItem(getItem(amount, item1[1].trim()));
				}
			}
			if (!kit_changing) usedkit.add(p);
		}
	}
	
	public void sendKitInfo(Player p, String kit) {
		if (getConfig().getStringList("kits." + kit + ".info") != null) {
			p.sendMessage(prefix + kit_info.replace("%kit%", kit).replace("%info%", ChatColor
					.translateAlternateColorCodes('&', getConfig().getString("kits." + kit + ".info"))));
		}
	}
	
	@SuppressWarnings("deprecation")
	public ItemStack getItem(int amount, String id) {
		if (id.contains(":")) {
			String[] id1 = id.split(":");
			Material mat = Material.getMaterial(Integer.parseInt(id1[0]));
			ItemStack is = new ItemStack(mat, amount, Short.parseShort(id1[1]));
			return is;
		} else {
			Material mat = Material.getMaterial(Integer.parseInt(id));
			ItemStack is = new ItemStack(mat, amount);
			return is;
		}
	}
	
	@SuppressWarnings("deprecation")
	public boolean isHelmet(String id) {
		if (id.contains(":")) {
			String[] id1 = id.split(":");
			Material mat = Material.getMaterial(Integer.parseInt(id1[0]));
			if (mat == (Material.LEATHER_HELMET) || mat == (Material.CHAINMAIL_HELMET) || mat == (Material.IRON_HELMET) || mat == (Material.GOLD_HELMET) || mat == (Material.DIAMOND_HELMET)) {
				return true;
			} else {
				return false;
			}
		} else {
			Material mat = Material.getMaterial(Integer.parseInt(id));
			if (mat == (Material.LEATHER_HELMET) || mat == (Material.CHAINMAIL_HELMET) || mat == (Material.IRON_HELMET) || mat == (Material.GOLD_HELMET) || mat == (Material.DIAMOND_HELMET)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public boolean isChestplate(String id) {
		if (id.contains(":")) {
			String[] id1 = id.split(":");
			Material mat = Material.getMaterial(Integer.parseInt(id1[0]));
			if (mat == (Material.LEATHER_CHESTPLATE) || mat == (Material.CHAINMAIL_CHESTPLATE) || mat == (Material.IRON_CHESTPLATE) || mat == (Material.GOLD_CHESTPLATE) || mat == (Material.DIAMOND_CHESTPLATE)) {
				return true;
			} else {
				return false;
			}
		} else {
			Material mat = Material.getMaterial(Integer.parseInt(id));
			if (mat == (Material.LEATHER_CHESTPLATE) || mat == (Material.CHAINMAIL_CHESTPLATE) || mat == (Material.IRON_CHESTPLATE) || mat == (Material.GOLD_CHESTPLATE) || mat == (Material.DIAMOND_CHESTPLATE)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public boolean isLeggings(String id) {
		if (id.contains(":")) {
			String[] id1 = id.split(":");
			Material mat = Material.getMaterial(Integer.parseInt(id1[0]));
			if (mat == Material.LEATHER_LEGGINGS || mat == Material.CHAINMAIL_LEGGINGS || mat == Material.IRON_LEGGINGS || mat == Material.GOLD_LEGGINGS || mat == Material.DIAMOND_LEGGINGS) {
				return true;
			} else {
				return false;
			}
		} else {
			Material mat = Material.getMaterial(Integer.parseInt(id));
			if (mat == Material.LEATHER_LEGGINGS || mat == Material.CHAINMAIL_LEGGINGS || mat == Material.IRON_LEGGINGS || mat == Material.GOLD_LEGGINGS || mat == Material.DIAMOND_LEGGINGS) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public boolean isBoots(String id) {
		if (id.contains(":")) {
			String[] id1 = id.split(":");
			Material mat = Material.getMaterial(Integer.parseInt(id1[0]));
			if (mat == (Material.LEATHER_BOOTS) || mat == (Material.CHAINMAIL_BOOTS) || mat == (Material.IRON_BOOTS) || mat == (Material.GOLD_BOOTS) || mat == (Material.DIAMOND_BOOTS)) {
				return true;
			} else {
				return false;
			}
		} else {
			Material mat = Material.getMaterial(Integer.parseInt(id));
			if (mat == (Material.LEATHER_BOOTS) || mat == (Material.CHAINMAIL_BOOTS) || mat == (Material.IRON_BOOTS) || mat == (Material.GOLD_BOOTS) || mat == (Material.DIAMOND_BOOTS)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public ConfigManager getConfigManager() {
		return configManager;
	}
	
/*	public void removeScoreboard(Player p) {
		ScoreboardManager manager = getServer().getScoreboardManager();
		Scoreboard sb = manager.getNewScoreboard();
		p.setScoreboard(sb);
	}
	
	public void updateScoreboard(Player p, boolean whole) {
		if (whole) {
			ScoreboardManager manager = getServer().getScoreboardManager();
			Scoreboard sb = manager.getNewScoreboard();
			Objective obj = sb.registerNewObjective("pvp", "dummy");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			obj.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Uprising PVP");
			//obj.getScore(" ").setScore(6);
			obj.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + "Points:").setScore(5);
			point.put(p, PointsAPI.getPoints(p));
			obj.getScore(ChatColor.AQUA + "" + point.get(p)).setScore(4);
			obj.getScore(" ").setScore(3);
			obj.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + "Rank:").setScore(2);
			rank.put(p, WordUtils.capitalizeFully(perms.getPrimaryGroup(p)));
			obj.getScore(ChatColor.AQUA + "" + rank.get(p)).setScore(1);
			p.setScoreboard(sb);
		} else if (!whole) {
			Objective obj = p.getScoreboard().getObjective(DisplaySlot.SIDEBAR); 
			p.getScoreboard().resetScores(ChatColor.AQUA + "" + point.get(p));
			point.put(p, PointsAPI.getPoints(p));
			obj.getScore(ChatColor.AQUA + "" + point.get(p)).setScore(4);
			p.getScoreboard().resetScores(ChatColor.AQUA + "" + rank.get(p));
			rank.put(p, WordUtils.capitalizeFully(perms.getPrimaryGroup(p)));
			obj.getScore(ChatColor.AQUA + "" + rank.get(p)).setScore(1);
		}
	}*/
}
