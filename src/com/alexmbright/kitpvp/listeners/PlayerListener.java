package com.alexmbright.kitpvp.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.alexmbright.kitpvp.Main;

public class PlayerListener implements Listener {
	private final Main plugin;

	public PlayerListener(Main instance) {
		this.plugin = instance;
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		for (String s : this.plugin.getConfig().getConfigurationSection("kits")
				.getKeys(false)) {
			if (this.plugin.getConfig().getBoolean("kits." + s + ".default")) {
				if (this.plugin.getConfig().getBoolean("kits." + s
							+ ".permission")) {
					if ((p.hasPermission(this.plugin.getConfig().getString(
							"kits." + s + ".permission-node")))
							|| (p.isOp())) {
						plugin.giveKit(p, s);
					} else {
						p.sendMessage(this.plugin.prefix
								+ plugin.no_permission);
					}
				} else {
					plugin.giveKit(p, s);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		if (plugin.getConfig().getBoolean("no-drops"))
			e.getDrops().clear();
		if (plugin.usedkit.contains(p))
			plugin.usedkit.remove(p);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (plugin.usedkit.contains(p)) {
			plugin.usedkit.remove(p);
			p.getInventory().clear();
		}
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e) {
		Player p = e.getPlayer();
		if (plugin.usedkit.contains(p)) {
			plugin.usedkit.remove(p);
			p.getInventory().clear();
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (e.getBlock().getState() instanceof Sign) {
			if (p.hasPermission("kits.sign.create") || p.isOp()) {
				Sign sign = (Sign) e.getBlock().getState();
				if (plugin.signs.getStringList("signs.grab").contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) {
					List<String> grabSigns = new ArrayList<String>();
					grabSigns.addAll(plugin.signs.getStringList("signs.grab"));
					if (grabSigns.contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) grabSigns.remove(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()));
					plugin.signs.set("signs.grab", grabSigns);
					plugin.getConfigManager().save();
					p.sendMessage(plugin.prefix + "�bGrab sign removed from config.");
				} else if (plugin.signs.getStringList("signs.info").contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) {
					List<String> infoSigns = new ArrayList<String>();
					infoSigns.addAll(plugin.signs.getStringList("signs.info"));
					if (infoSigns.contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) infoSigns.remove(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()));
					plugin.signs.set("signs.info", infoSigns);
					plugin.getConfigManager().save();
					p.sendMessage(plugin.prefix + "�bInfo sign removed from config.");
				}
			} else {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		Player p = e.getPlayer();
		if ((e.getLine(0).equalsIgnoreCase("[kitpvp]"))
				&& (e.getLine(2).equalsIgnoreCase("grab"))) {
			if ((p.hasPermission("kits.sign.create")) || (p.isOp())) {
				final String kit = e.getLine(1);
				if (plugin.isKit(kit)) {
					e.setLine(0, ChatColor.translateAlternateColorCodes('&', plugin.grab_layout.get(0).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(1, ChatColor.translateAlternateColorCodes('&', plugin.grab_layout.get(1).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(2, ChatColor.translateAlternateColorCodes('&', plugin.grab_layout.get(2).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(3, ChatColor.translateAlternateColorCodes('&', plugin.grab_layout.get(3).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					if (plugin.signs.contains("signs.grab")) {
						List<String> grabSigns = new ArrayList<String>();
						grabSigns.addAll(plugin.signs.getStringList("signs.grab"));
						grabSigns.add(String.valueOf(e.getBlock().getX()) + String.valueOf(e.getBlock().getY()) + String.valueOf(e.getBlock().getZ()));
						plugin.signs.set("signs.grab", grabSigns);
						plugin.getConfigManager().save();
					} else {
						List<String> grabSigns = new ArrayList<String>();
						grabSigns.add(String.valueOf(e.getBlock().getX()) + String.valueOf(e.getBlock().getY()) + String.valueOf(e.getBlock().getZ()));
						plugin.signs.set("signs.grab", grabSigns);
						plugin.getConfigManager().save();
					}
				} else {
					e.setLine(0, "�a[error]");
					e.setLine(1, "�4not a kit");
				}
			} else {
				e.setLine(0, "�a[error]");
				e.setLine(1, "�4no permission");
			}
		} else if ((e.getLine(0).equalsIgnoreCase("[kitpvp]"))
				&& (e.getLine(2).equalsIgnoreCase("info"))) {
			if ((p.hasPermission("kits.sign.create")) || (p.isOp())) {
				final String kit = e.getLine(1);
				if (plugin.isKit(kit)) {
					e.setLine(0, ChatColor.translateAlternateColorCodes('&', plugin.info_layout.get(0).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(1, ChatColor.translateAlternateColorCodes('&', plugin.info_layout.get(1).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(2, ChatColor.translateAlternateColorCodes('&', plugin.info_layout.get(2).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					e.setLine(3, ChatColor.translateAlternateColorCodes('&', plugin.info_layout.get(3).replace("%kit%", kit).replace("%special%", this.plugin.getConfig().getBoolean(
							"kits." + kit + ".permission") ? plugin.special_tag : "")));
					if (plugin.signs.contains("signs.info")) {
						List<String> infoSigns = new ArrayList<String>();
						infoSigns.addAll(plugin.signs.getStringList("signs.info"));
						infoSigns.add(String.valueOf(e.getBlock().getX()) + String.valueOf(e.getBlock().getY()) + String.valueOf(e.getBlock().getZ()));
						plugin.signs.set("signs.info", infoSigns);
						plugin.getConfigManager().save();
					} else {
						List<String> infoSigns = new ArrayList<String>();
						infoSigns.add(String.valueOf(e.getBlock().getX()) + String.valueOf(e.getBlock().getY()) + String.valueOf(e.getBlock().getZ()));
						plugin.signs.set("signs.info", infoSigns);
						plugin.getConfigManager().save();
					}
				} else {
					e.setLine(0, "�a[error]");
					e.setLine(1, "�4not a kit");
				}
			} else {
				e.setLine(0, "�a[error]");
				e.setLine(1, "�4no permission");
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if ((e.getAction() == Action.RIGHT_CLICK_BLOCK)
				&& ((e.getClickedBlock().getState() instanceof Sign))) {
			Sign sign = (Sign) e.getClickedBlock().getState();
			if (plugin.signs.getStringList("signs.grab").contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) {
				if (plugin.isKit(ChatColor.stripColor(sign.getLine(0)))) {
					p.performCommand("kit " + ChatColor.stripColor(sign.getLine(0)));
					p.updateInventory();
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(1)))) {
					p.performCommand("kit " + ChatColor.stripColor(sign.getLine(1)));
					p.updateInventory();
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(2)))) {
					p.performCommand("kit " + ChatColor.stripColor(sign.getLine(2)));
					p.updateInventory();
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(3)))) {
					p.performCommand("kit " + ChatColor.stripColor(sign.getLine(3)));
					p.updateInventory();
				} else {
					p.sendMessage("�cThis is a corrupt sign layout. Please report this error message to the server administrator.");
				}
			} else if (plugin.signs.getStringList("signs.info").contains(String.valueOf(sign.getBlock().getX()) + String.valueOf(sign.getBlock().getY()) + String.valueOf(sign.getBlock().getZ()))) {
				if (plugin.isKit(ChatColor.stripColor(sign.getLine(0)))) {
					plugin.sendKitInfo(p, ChatColor.stripColor(sign.getLine(0)));
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(1)))) {
					plugin.sendKitInfo(p, ChatColor.stripColor(sign.getLine(1)));
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(2)))) {
					plugin.sendKitInfo(p, ChatColor.stripColor(sign.getLine(2)));
				} else if (plugin.isKit(ChatColor.stripColor(sign.getLine(3)))) {
					plugin.sendKitInfo(p, ChatColor.stripColor(sign.getLine(3)));
				} else {
					p.sendMessage("�cThis is a corrupt sign layout. Please report this error message to the server administrator.");
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent ev) {
		if (plugin.blood) { 
			Entity entity1 = ev.getEntity();
			Entity entity2 = ev.getDamager();
			World world = ev.getDamager().getWorld();
			if ((entity1 instanceof Player)) {
				Player defender = (Player) entity1;
				if ((entity2 instanceof Player)) {
					Location hitLoc = new Location(defender.getWorld(), defender
							.getLocation().getX(),
							defender.getLocation().getY() + 0.5D, defender
									.getLocation().getZ());
					world.playEffect(hitLoc, Effect.STEP_SOUND,
							Material.REDSTONE_WIRE.getId(), 20);
				}
			}
		}
	}
	 
}
